//
//  Enums.swift
//  ScorPeople
//
//  Created by Abdullah Okudan on 6.10.2021.
//

import Foundation

enum ViewModelState: Equatable {
    case loading
    case error(String)
    case idle
}
