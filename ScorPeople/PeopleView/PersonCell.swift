//
//  PeopleCell.swift
//  ScorPeople
//
//  Created by Abdullah Okudan on 6.10.2021.
//

import UIKit

class PersonCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var indicator: UIActivityIndicatorView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configure(with: .none)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(with person: Person?){
        if let person = person {
            lblName?.text = "\(person.fullName) (\(person.id))"
            lblName?.alpha = 1
            indicator?.stopAnimating()
        }else{
            lblName?.alpha = 0
            indicator?.startAnimating()
        }
    }
}
