//
//  ViewModel.swift
//  ScorPeople
//
//  Created by Abdullah Okudan on 6.10.2021.
//

import Foundation

protocol PeopleViewModelDelegate: AnyObject {
    func hideEmptyMessage(hide: Bool)
    func onFetchCompleted(with newIndexPathsToReload: [IndexPath]?)
    func updateUI()
    func didUpdateState()
}

class PeopleViewModel {
    weak var delegate: PeopleViewModelDelegate?
    var state: ViewModelState = .idle {
        didSet {
            delegate?.didUpdateState()
        }
    }
    
    private var total = 0
    private var isFirstPage: Bool = true
    private var nextPage: String?
    private var people: [Person] = []
    
    var totalCount: Int {
        return total
    }
    
    var currentCount: Int {
        return people.count
    }
    
    func viewDidLoad() {
        delegate?.updateUI()
        fetchPeople()
    }
    
    func doTopRefresh() {
        if currentCount == 0 {
            fetchPeople()
        }else{
            state = .idle
        }
    }
    
    func person(at index: Int) -> Person {
        return people[index]
    }
    
    func fetchPeople(){
        if state == .loading {
            return
        }
        
        state = .loading
        
        DispatchQueue.background(delay: 0.05, completion:{
            DataSource.fetch(next: self.nextPage) { fResponse, fError in
                
                if let error = fError {
                    self.state = .error(error.errorDescription)
                }else if let response = fResponse {
                    self.nextPage = response.next
    
                    self.createNewPeopleData(responsePeople: response.people)
                    self.checkForNext(response.next)
                    
                    self.state = .idle
                }
            }
        })
    }
    
    private func createNewPeopleData(responsePeople: [Person]){
        if responsePeople.count > 0 {
            let distinctPeople = self.findDistinctPeople(persons: responsePeople)
            self.people.append(contentsOf: distinctPeople)
            self.total = self.people.count + 20
            
            self.showNewPeople(distinctPeople: distinctPeople)
        }
    }
    
    private func findDistinctPeople(persons: [Person]) -> [Person]{
        var tempPersons: [Person] = []
        for p in persons {
            if !people.contains(where: {$0.id == p.id}) {
                tempPersons.append(p)
            }
        }
        return tempPersons
    }
    
    private func showNewPeople(distinctPeople: [Person]){
        if self.isFirstPage {
            self.delegate?.onFetchCompleted(with: .none)
        }else{
            self.isFirstPage = false
            let indexPathsToReload = self.calculateIndexPathsToReload(from: distinctPeople)
            self.delegate?.onFetchCompleted(with: indexPathsToReload)
        }
    }
    
    private func calculateIndexPathsToReload(from newPeople: [Person]) -> [IndexPath] {
        let startIndex = self.people.count - newPeople.count
        let endIndex = startIndex + newPeople.count
        return (startIndex..<endIndex).map { IndexPath(row: $0, section: 0) }
    }
    
    private func checkForNext(_ next: String?){
        if next == nil {
            self.total = self.people.count
            self.delegate?.onFetchCompleted(with: .none)
            
            self.delegate?.hideEmptyMessage(hide: (self.currentCount > 0) ? true : false)
        }else{
            self.delegate?.hideEmptyMessage(hide: true)
        }
    }
}
