//
//  ViewController.swift
//  ScorPeople
//
//  Created by Abdullah Okudan on 6.10.2021.
//

import UIKit

class PeopleViewController: UIViewController {
    
    @IBOutlet weak var tblPeople: UITableView?
    @IBOutlet weak var lblEmpty: UILabel?
    
    let viewModel = PeopleViewModel()
    
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        viewModel.viewDidLoad()
    }
    
    @objc func refresh(_ sender: AnyObject) {
        viewModel.doTopRefresh()
    }
}

extension PeopleViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.totalCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PersonCell", for: indexPath) as! PersonCell
        
        
        if isLoadingCell(for: indexPath) {
            cell.configure(with: .none)
        } else {
            cell.configure(with: viewModel.person(at: indexPath.row))
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension PeopleViewController: UITableViewDataSourcePrefetching{
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        if indexPaths.contains(where: isLoadingCell) {
            viewModel.fetchPeople()
        }
    }
}

extension PeopleViewController: PeopleViewModelDelegate{
    func updateUI() {
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        tblPeople?.dataSource = self
        tblPeople?.delegate = self
        tblPeople?.prefetchDataSource = self
        
        tblPeople?.refreshControl = refreshControl
        tblPeople?.insertSubview(refreshControl, at: 0)
        
        tblPeople?.tableFooterView = UIView.init(frame: .zero)
    }
    
    func hideEmptyMessage(hide: Bool){
        lblEmpty?.isHidden = hide
    }
    
    func onFetchCompleted(with newIndexPathsToReload: [IndexPath]?){
        guard let newIndexPathsToReload = newIndexPathsToReload else {
            tblPeople?.reloadData()
            return
        }
        
        let indexPathsToReload = visibleIndexPathsToReload(intersecting: newIndexPathsToReload)
        tblPeople?.reloadRows(at: indexPathsToReload, with: .automatic)
    }
    
    func didUpdateState() {
        switch viewModel.state {
        case .idle:
            refreshControl.endRefreshing()
        case .loading:
            refreshControl.beginRefreshing()
        case .error( _):
            viewModel.fetchPeople()
        }
    }
}

private extension PeopleViewController {
    func isLoadingCell(for indexPath: IndexPath) -> Bool {
        return indexPath.row >= viewModel.currentCount
    }
    
    func visibleIndexPathsToReload(intersecting indexPaths: [IndexPath]) -> [IndexPath] {
        let indexPathsForVisibleRows = tblPeople?.indexPathsForVisibleRows ?? []
        let indexPathsIntersection = Set(indexPathsForVisibleRows).intersection(indexPaths)
        return Array(indexPathsIntersection)
    }
}

